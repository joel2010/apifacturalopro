import Vue from "vue";
import App from "./App.vue";
import router from "./routes/router";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import axios from "axios";
axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
let token = document.head.querySelector('meta[name="csrf-token"]');
axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
Vue.prototype.$axios = axios;

new Vue({
    el: "#app",
    render: h => h(App),
    router
});
