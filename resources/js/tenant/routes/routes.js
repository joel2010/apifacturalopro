import Login from "../components/Login.vue";
import Dashboard from "../components/Dashboard.vue";
import Todo from "../components/Todo.vue";

const routes = [
    {
        path: "/",
        redirect: "/login"
    },
    {
        path: "/dashboard",
        component: Dashboard,
        children: [
            {
                path: "/",
                name: 'dashboard',
                component: Todo
            }
        ]
    },
    {
        path: "/login",
        name: "login",
        component: Login
    }
];

export default routes;
