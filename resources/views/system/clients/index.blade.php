@extends('layouts.app')

@section('content')
    <apifact-clients :clients='@json($clients)'></apifact-clients>
@endsection
