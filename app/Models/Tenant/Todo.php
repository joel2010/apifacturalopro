<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Abstracts\TenantModel;

class Todo extends TenantModel
{
	protected $table = 'todo';

	protected $fillable = ['name', 'description', 'completed', 'priority'];

	public function getCompletedAttribute($value)
	{
		return $value ? true : false;
	}
}
