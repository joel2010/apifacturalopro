<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Abstracts\TenantModel;

class User extends TenantModel
{
    protected $hidden = [
        'password',
    ];
}
