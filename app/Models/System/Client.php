<?php

namespace App\Models\System;

use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Abstracts\SystemModel;

class Client extends SystemModel
{
	public function hostname()
	{
		return $this->belongsTo(Hostname::class);
	}

	public function website()
	{
		return $this->belongsTo(Website::class);
	}
}
