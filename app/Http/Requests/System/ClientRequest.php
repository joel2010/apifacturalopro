<?php

namespace App\Http\Requests\System;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->fqdn = $this->fqdn . '.' . env('APP_URL_BASE');
        return [
            'fqdn' => ['required', 'max:255', 'alpha_dash', Rule::unique('hostnames', 'fqdn')->ignore($this->id)],
            'fullname' => ['required', 'max:120', 'string', Rule::unique('clients')->ignore($this->id)],
            'email' => ['required', 'max:190', 'email', Rule::unique('clients')->ignore($this->id)],
            'password' => 'sometimes|required|min:6|confirmed',
        ];
    }
}
