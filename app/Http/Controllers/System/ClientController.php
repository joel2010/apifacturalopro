<?php

namespace App\Http\Controllers\System;

use App\Models\System\Client;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Models\Hostname;
use App\Http\Controllers\Controller;
use App\Http\Requests\System\ClientRequest;
use App\Models\Tenant\User;
use Hyn\Tenancy\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;

class ClientController extends Controller
{
	public function index()
	{
		$clients = Client::with('hostname', 'website')
			->orderBy('id', 'DESC')
			->get();

		return view('system.clients.index', compact('clients'));
	}

	public function store(ClientRequest $request)
	{
		$website        = new Website();
		$hostname       = new Hostname();
		$hostname->fqdn = $request->fqdn;
		$hostname       = app(HostnameRepository::class)->create($hostname);

		app(WebsiteRepository::class)->create($website);
		app(HostnameRepository::class)->attach($hostname, $website);

		$client         = $this->generateClientData($hostname->id, $website->id, new Client(), $request);
		$client->save();
		$client->load(['website', 'hostname']);

		$this->addUserToTenant($request);

		return response()->json([
			'data'    => $client,
			'success' => true,
			'message' => 'Información actualizada'
		], 200);
	}

	public function update(ClientRequest $request, $id)
	{
		$client           = Client::findOrFail($id);
		$client->fullname = $request->fullname;
		$client->save();

		$client->load(['website', 'hostname']);

		return response()->json([
			'data'    => $client,
			'success' => true,
			'message' => 'Información actualizada'
		], 200);
	}

	public function destroy($id)
	{
		$client = Client::find($id);

		$hostname = Hostname::find($client->hostname_id);
		$website  = Website::find($hostname->website_id);

		app(HostnameRepository::class)->delete($hostname, true);
		app(WebsiteRepository::class)->delete($website, true);

		return response()->json([
			'data'    => null,
			'success' => true,
			'message' => 'Cliente eliminado de forma correcta'
		], 200);
	}

	private function addUserToTenant(ClientRequest $request)
	{
		$user           = new User();
		$user->fullname = $request->fullname;
		$user->email    = $request->email;
		if ($request->has('password') && $request->filled('password')) {
			$user->password = bcrypt($request->password);
		}
		$user->save();
	}

	private function generateClientData(
		int $hostnameId = null,
		int $websiteId = null,
		Client $client,
		ClientRequest $request
	): Client {
		if ($hostnameId) {
			$client->hostname_id = $hostnameId;
		}
		if ($websiteId) {
			$client->website_id = $websiteId;
		}
		$client->fullname = $request->fullname;
		$client->email    = $request->email;

		return $client;
	}
}
