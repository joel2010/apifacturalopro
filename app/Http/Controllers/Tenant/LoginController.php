<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Tenant\LoginRequest;

class LoginController extends Controller
{
	public function login(LoginRequest $request)
	{
		$credentials = $request->only('email', 'password');

		if (Auth::attempt($credentials)) {
			return response()->json([
				'success' => true,
				'data'    => [
					'csrf_token' => csrf_token(),
				],
				'message' => 'Usuario logueado de forma correcta.'
			], 200);
		}

		return response()->json([
			'success' => false,
			'message' => 'Usuario o contraseña incorrectos.'
		], 500);
	}

	public function isLogged()
	{
		if (Auth::check()) {
			return response()->json([
				'success' => true,
				'message' => 'Usuario logueado.'
			], 200);
		}

		return response()->json([
			'success' => false,
			'message' => 'No tienes permiso para entrar a esta sección.'
		], 200);
	}

	public function logout()
	{
		Auth::logout();

		return response()->json([
			'success' => true,
            'message' => 'Sesión cerrada de forma correcta.',
            'data' => [
                'redirect' => route('tenant.home')
            ]
		], 200);
	}
}
