<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tenant\TodoRequest;
use App\Models\Tenant\Todo;

class TodoController extends Controller
{
	public function index()
	{
		$todo = Todo::orderBy('id', 'DESC')
			->get();

		return response()->json([
			'data'    => $todo,
			'success' => true,
			'message' => 'Tareas listadas de forma correcta.'
		], 200);
	}

	public function store(TodoRequest $request)
	{
		$todo = Todo::create($request->only('name', 'description', 'completed', 'priority'));

		return response()->json([
			'data'    => $todo,
			'success' => true,
			'message' => 'Tarea creada de forma correcta.'
		], 200);
	}

	public function update(TodoRequest $request, $id)
	{
		$todo = Todo::findOrFail($id);
		$todo->fill($request->only('name', 'description', 'completed', 'priority'));
		$todo->save();

		return response()->json([
			'data'    => $todo,
			'success' => true,
			'message' => 'Tarea actualizada de forma correcta.'
		], 200);
	}

	public function destroy($id)
	{
		Todo::where('id', $id)->delete();

		return response()->json([
			'data'    => null,
			'success' => true,
			'message' => 'La tarea fue eliminada de forma correcta.'
		], 200);
	}
}
