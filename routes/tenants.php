<?php

use Illuminate\Support\Facades\Route;

Route::middleware('web')
	->namespace('App\Http\Controllers\Tenant')
	->group(function () {
        Route::get('/', 'HomeController@index')->name('tenant.home');

        Route::get('auth/is-logued', 'LoginController@isLogged');
        Route::post('auth/login', 'LoginController@login');
        Route::post('auth/logout', 'LoginController@logout');

        Route::group(['middleware' => 'auth', 'prefix' => 'dashboard'], function () {
            Route::get('todo', 'TodoController@index');
            Route::post('todo/store', 'TodoController@store');
            Route::put('todo/{id}/update', 'TodoController@update');
            Route::delete('todo/{id}/delete', 'TodoController@destroy');
        });
    });

