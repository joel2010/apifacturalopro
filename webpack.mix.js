const mix = require("laravel-mix");

mix.js("resources/js/app.js", "public/js")
    .sass("resources/sass/style.scss", "public/css/app.css")
    .sass("resources/js/tenant/sass/tenant.scss", "public/css")
    .js("resources/js/tenant/tenant.js", "public/js")
    .version();
